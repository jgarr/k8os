k8os
====

Turn your old computers into a Kubernetes cluster! :wheel_of_dharma: :desktop: :raised_hands:
This project is opinionated to install Kubernetes on metal as simple as possible without any external dependencies (PXE, etc.).

Booting a computer from this USB **WILL** erase the hard drive and install CentOS.

## Getting Started

Write the latest iso image directly to a USB drive

**Warning:** This will erase the first USB drive found on your system

On Linux
```
curl -L 'https://gitlab.com/jgarr/k8os/-/jobs/artifacts/v1.3/raw/k8os.iso?job=build' \
    | sudo dd of="$(lsblk -p -S -o  NAME,TRAN | grep usb | awk '{print $1}')" bs=4M && sync
```

On macOS
```
export USB_DRIVE=$(diskutil list | grep 'external, physical' | awk '{print $1}') \
    && sudo diskutil unmount $USB_DRIVE; \
    curl -L 'https://gitlab.com/jgarr/k8os/-/jobs/artifacts/v1.3/raw/k8os.iso?job=build' \
    | sudo dd of="$USB_DRIVE" bs=4m && sync
```

1. Boot spare computer from USB (first computer will be Kubernetes API)
2. Download KUBECONFIG file `curl -o ~/.kube/config https://kubernetes:8443/admin.conf`
3. (optional) Boot Nth computer from USB drive to add worker nodes
4. Have fun :thumbsup:

### Manually create your own ISO

Plug in a USB drive.

To create the ISO you can clone the repo and run `make`

1. `git clone https://gitlab.com/jgarr/k8os.git`
2. `cd k8os`
3. `make`

You can manually `dd` the iso to a USB drive or burn it to an actual disc.
If you want to write the iso to a USB drive automatically you can use `make usb`.

## What this is

The easiest way to turn a spare computer(s) into a Kubernetes cluster.

* It uses the [kubeadm](https://github.com/kubernetes/kubeadm) project to bootstrap the nodes
* It uses [weave](https://github.com/weaveworks/weave) as the default CNI
* It uses docker as the container runtime

## What this is not

This is designed to be used in a development environment **ONLY**.

* It is not, and never will be, a production grade Kubernetes installer
* It is not a secure cluster (e.g. the default root password is `password`)
* It is not a highly available installation and does not support multiple masters
* It only supports x86 hardware

## Information

The OS that is created is based on CentOS Minimal installation.
The installation is automated using the ks.cfg file in this repo.

By default it will install the latest stable release of Kubernetes.

## Troubleshooting

If provisioning fails make sure the system has internet access during the installation.
It will install the latest packages from CentOS repos.

The first node that boots will be the master node.
It will take the hostname `kubernetes` and all other nodes will look for that host to be available when they boot.
If DNS does not work on your network this installation will not work.

You can SSH into the hosts after provisioning with `root` and `password`

If the master does not provision properly check the `kubeadm-init` service with `systemctl status kubeadm-init` or `journalctl -flu kubeadm-init`.

If a worker doesn't join the cluster check for the `kubeadm-join` service.

All services should be resilient to a reboot or system being powered off but YMMV.

## ToDo

- [ ] Support running `make` from macOS and Debian based distros
- [ ] Support specifying the version of Kubernetes you'd like to install
- [ ] Support adding ssh keys
- [ ] Allow alternative CNI options
- [ ] Enable addons (ingress, heapster, etc.)
