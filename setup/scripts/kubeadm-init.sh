#!/bin/bash

# needed for kube-proxy
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

kubeadm init --token $(cat /srv/k8s/kubeadm.token) \
  --token-ttl 0 \
  $KUBEADM_INIT_OPTIONS

# Allow the master to run pods
export KUBECONFIG=/etc/kubernetes/admin.conf
kubectl taint nodes --all node-role.kubernetes.io/master-
