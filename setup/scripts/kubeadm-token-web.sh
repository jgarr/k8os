#!/bin/bash

mkdir -p /srv/nginx

# need an nginx conf that supports TLS
cat <<EOF >/srv/nginx/nginx.conf
server {
    listen       8443 ssl;
    server_name  localhost;

    ssl_certificate      /etc/nginx/ssl/apiserver.crt;
    ssl_certificate_key  /etc/nginx/ssl/apiserver.key;

    location / {
        root   /usr/share/nginx/html;
    }

    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
        root   /usr/share/nginx/html;
    }
}
EOF

# wait for the service account to be ready
until /usr/bin/kubectl --kubeconfig=/etc/kubernetes/admin.conf --namespace=kube-system describe serviceaccount default; do
    printf '.'
    sleep 5
done

# Write out a static pod for generated token
cat <<EOF >/srv/k8s/kubeadm-token-web.yaml
apiVersion: v1
kind: Pod
metadata:
  name: kubeadm-bootstrap
spec:
  hostNetwork: true
  containers:
  - name: kubeadm-bootstrap
    image: nginx:alpine
    volumeMounts:
    - mountPath: /etc/nginx/ssl/
      name: ssl-volume
      readOnly: true
    - mountPath: /etc/nginx/conf.d/
      name: nginx-conf
      readOnly: true
    - mountPath: /usr/share/nginx/html
      name: kubeadm-token
      readOnly: true
  volumes:
  - name: ssl-volume
    hostPath:
      path: /etc/kubernetes/pki
  - name: nginx-conf
    hostPath:
      path: /srv/nginx
  - name: kubeadm-token
    hostPath:
      path: /srv/k8s
EOF
kubectl create --kubeconfig=/etc/kubernetes/admin.conf -f /srv/k8s/kubeadm-token-web.yaml

# Save active IP address
IP_ADDR=$(ip addr | awk '
/^[0-9]+:/ {
  sub(/:/,"",$2); iface=$2 }
/^[[:space:]]*inet / {
  split($2, a, "/")
  print iface" : "a[1]
}' \
  | grep -v 'lo\|docker\|weave' \
  | awk -F " : " '{print $2}')

# create a new config file useful in case the master IP changes
sed "s/${IP_ADDR}/kubernetes/g" /etc/kubernetes/admin.conf > /srv/k8s/config
