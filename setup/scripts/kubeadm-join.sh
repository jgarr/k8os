#!/bin/bash

# needed for kube-proxy
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables

# Wait for master to be available
until $(curl --output /dev/null --silent --head --fail -k https://kubernetes:8443/kubeadm.token); do
    printf '.'
    sleep 5
done

kubeadm join --discovery-token-unsafe-skip-ca-verification --token $(curl --silent -k https://kubernetes:8443/kubeadm.token) kubernetes:6443
