#!/bin/bash

# Generate a token to use later
mkdir -p /srv/k8s
kubeadm token generate > /srv/k8s/kubeadm.token

