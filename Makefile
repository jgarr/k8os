# Makefile for k8os
.DEFAULT_GOAL := k8os.iso

PWD = $(shell pwd)
CENTOS_VER ?= 1708
CENTOS_ISO ?= CentOS-7-x86_64-NetInstall-$(CENTOS_VER).iso
ISO_DIR ?= isodir
KB_LAYOUT ?= us
POST_ACTION ?= shutdown
KUBEADM_INIT_OPTIONS ?= ''
ISO_URL ?= http://mirror.steadfast.net/centos/7/isos/x86_64/$(CENTOS_ISO)
USB_DEV ?= $(shell lsblk -p -S -o  NAME,TRAN | grep usb | awk '{print $$1}')
ifndef USB_DEV
	?= $(shell read -p "Enter the USB device: " usb_dev; stty -echo; echo $$usb_dev)
endif
UNAME_S := $(shell uname -s)
DISTRO := $(shell lsb_release -si)
GENISOIMAGE := $(shell command -v genisoimage 2> /dev/null)
7Z := $(shell command -v 7z 2> /dev/null)

k8os.iso: $(ISO_DIR)
# do detection for Linux and Mac
ifeq ($(UNAME_S),Linux)
ifeq ($(DISTRO),$(filter $(DISTRO),Fedora CentOS))
ifndef GENISOIMAGE
	sudo yum install -y genisoimage
endif
ifndef 7Z
	sudo yum install -y p7zip-full
endif
endif
ifeq ($(UNAME_S),$(filter $(DISTRO),Debian Ubuntu))
ifndef GENISOIMAGE
	sudo apt install -y genisoimage
endif
ifndef 7Z
	sudo apt install -y p7zip-full
endif
endif
endif
ifeq ($(UNAME_S),Darwin)
ifndef GENISOIMAGE
	brew install cdrtools
endif
endif
	DOLLAR="$$" KUBEADM_INIT_OPTIONS=$(KUBEADM_INIT_OPTIONS) envsubst < setup/etc/kubeadm.env.template > ./setup/etc/kubeadm.env
	mkisofs -o k8os.iso -b isolinux.bin -c boot.cat -no-emul-boot -V 'k8os' \
		-boot-load-size 4 -boot-info-table -R -J -v -T \
		$(ISO_DIR)/isolinux/ setup/
	isohybrid k8os.iso

$(CENTOS_ISO):
	wget -N $(ISO_URL)

sha256sum.txt: $(CENTOS_ISO)
	curl -s http://mirror.centos.org/centos/7/isos/x86_64/sha256sum.txt.asc \
		| grep $(CENTOS_ISO) > sha256sum.txt

.PHONY: verify_iso
verify_iso: sha256sum.txt
	sha256sum -c sha256sum.txt

$(ISO_DIR): verify_iso
	mkdir -p ./extract
	7z x $(CENTOS_ISO) -oextract
	mkdir -p ./$(ISO_DIR)/isolinux/{images,ks,LiveOS}
	rsync -av ./extract/isolinux/* ./$(ISO_DIR)/isolinux/
	rsync -av ./extract/images/ ./$(ISO_DIR)/isolinux/images/
	rsync -av ./extract/LiveOS/* ./$(ISO_DIR)/isolinux/LiveOS/
	KB_LAYOUT=$(KB_LAYOUT) POST_ACTION=$(POST_ACTION) DOLLAR="$$" envsubst < ks.cfg >./$(ISO_DIR)/isolinux/ks/ks.cfg
	# delete default boot
	sed -i '/^\s*menu\ default/d' ./$(ISO_DIR)/isolinux/isolinux.cfg
	# add isolinux to linux menu
	sed -i '/^label linux/{N;N;N;s,img inst\.stage2,img inst.ks=hd:LABEL=CentOS\\x207\\x20x86_64:/ks/ks.cfg inst\.sshd=1 inst\.stage2,}' ./$(ISO_DIR)/isolinux/isolinux.cfg
	# add default to linux
	sed -i '/^label linux/{N;N;s,$$,\n  menu default,}' ./$(ISO_DIR)/isolinux/isolinux.cfg
	# shorten timeout
	sed -i 's/^timeout.*$$/timeout 50/g' ./$(ISO_DIR)/isolinux/isolinux.cfg
	# rebrand iso
	sed -i 's/CentOS 7/k8os/g' ./$(ISO_DIR)/isolinux/isolinux.cfg
	sed -i 's/CentOS\\x207\\x20x86_64/k8os/g' ./$(ISO_DIR)/isolinux/isolinux.cfg

.PHONY: usb
usb: k8os.iso
	@echo '-------------------------------------'
	@while [ -z "$$CONTINUE" ]; do \
        read -r -p "Would you like to continue? This will wipe $(USB_DEV) [y/N]: " CONTINUE; \
    done ; \
    [ $$CONTINUE = "y" ] || [ $$CONTINUE = "Y" ] || (echo "Exiting."; exit 1;)
	dd if=k8os.iso | pv | sudo dd of="$(USB_DEV)" bs=4M
	sync

.PHONE: clean
clean:
	rm -rf k8os.iso extract $(ISO_DIR) setup/etc/kubeadm.env

.PHONE: cleanall
cleanall: clean
	rm -rf $(CENTOS_ISO) sha256sum.txt
